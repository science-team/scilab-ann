;##############################################################################################################
; Inno Setup Install script for Toolbox_skeleton
; http://www.jrsoftware.org/isinfo.php
; Allan CORNET
; Copyright INRIA 2008
;##############################################################################################################
; modify this path where is toolbox_skeleton directory
#define BinariesSourcePath "E:\ANN_Toolbox_0.4.2.2"
#define ANN_Toolbox_version "0.4.2.2"
#define CurrentYear "2008"
#define Toolbox_ANNDirFilename "ANN_Toolbox_0.4.2.2"
;##############################################################################################################
[Setup]
; Debut Données de base à renseigner suivant version
SourceDir={#BinariesSourcePath}
AppName=ANN Toolbox 0.4.2.2 for Scilab 5.x
AppVerName=ANN Toolbox 0.4.2.2 for Scilab 5.x
DefaultDirName={pf}\{#Toolbox_ANNDirFilename}
InfoAfterfile=readme.txt
LicenseFile=license.txt
WindowVisible=true
AppPublisher=Your Company
BackColorDirection=lefttoright
AppCopyright=Copyright © {#CurrentYear}
Compression=lzma/max
InternalCompressLevel=normal
SolidCompression=true
VersionInfoVersion={#ANN_Toolbox_version}
VersionInfoCompany=NONE
;##############################################################################################################
[Files]
; Add here files that you want to add
Source: loader.sce; DestDir: {app}
Source: builder.sce; DestDir: {app}
Source: license.txt; DestDir: {app}
Source: etc\ANN_toolbox.quit; DestDir: {app}\etc
Source: etc\ANN_toolbox.start; DestDir: {app}\etc
Source: help\en_US\addchapter.sce; DestDir: {app}\help\en_US
Source: jar\scilab_en_US_help.jar; DestDir: {app}\jar
Source: macros\buildmacros.sce; DestDir: {app}\macros
Source: macros\lib; DestDir: {app}\macros
Source: macros\names; DestDir: {app}\macros
Source: macros\*.sci; DestDir: {app}\macros
Source: macros\*.bin; DestDir: {app}\macros
Source: demos\*.*; DestDir: {app}\demos; Flags: recursesubdirs
;
;##############################################################################################################
;
