// ==================================================
// Tight 4-2-4 encoder using a mixed standard/conjugate gradients algorithm
// ==================================================
FILENAMEDEM = "encoder_cc";
lines(0);
scepath = get_absolute_file_path(FILENAMEDEM+".sce");
exec(scepath+FILENAMEDEM+".sci",1);
clear scepath;
clear FILENAMEDEM;
// ==================================================
