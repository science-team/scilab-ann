// ==================================================
// Tight 4-2-4 encoder on a backpropagation ANN
// ==================================================
FILENAMEDEM = "encoder";
lines(0);
scepath = get_absolute_file_path(FILENAMEDEM+".sce");
exec(scepath+FILENAMEDEM+".sci",1);
clear scepath;
clear FILENAMEDEM;
// ==================================================
