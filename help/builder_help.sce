// ====================================================================
// Copyright INRIA 2008
// Allan CORNET
// ====================================================================

tbx_builder_help_lang(["en_US"], ..
                      get_absolute_file_path("builder_help.sce"));
