#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=ann-toolbox
DIR_TARGET=scilab-ann-$2
TAR=../scilab-ann_$2.orig.tar.gz

# clean up the upstream tarball
unzip $3
mv $DIR $DIR_TARGET
tar -c -z -f $TAR $DIR_TARGET
rm -rf $DIR $3 $DIR_TARGET

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
    . .svn/deb-layout
    mv $TAR $origDir
    echo "moved $TAR to $origDir"
fi

exit 0
